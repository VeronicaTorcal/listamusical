package com.example.veriko.listamusical;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // declaramos el listview, si no no s ve nada
    ListView listaCanciones;

    // creamos el array de objetos Cancion
    ArrayList<Cancion> datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // instanciamos el listview
        listaCanciones = (ListView)findViewById(R.id.listacanciones);
        datos= new ArrayList<Cancion>();

        datos.add(new Cancion(R.drawable.bonjovi, "Livin on a Prayer", "Bon Jovi", "4.37", R.raw.livin_on_a_prayer));
        datos.add(new Cancion(R.drawable.cindylauper,"She's so unusual","Cindy Lauper", "3:33", R.raw.girls_just_want_to_have_fun));
        datos.add(new Cancion(R.drawable.madonna,"Like a Prayer","Madonna", "4:15", R.raw.like_a_prayer));
        datos.add(new Cancion(R.drawable.queen,"We are the Champions","Queen","4:00", R.raw.we_are_the_champions));

        Adaptador adaptador=new Adaptador(this, datos);
        listaCanciones.setAdapter(adaptador);
        // hasta aqui sale la lista de canciones, pero aun no tiene boton para ir a cada una
        listaCanciones.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i=new Intent(getApplicationContext(), Reproducir.class);
                i.putExtra("objCancion", datos.get(position));
                startActivity(i);
            }
        });

    }
}
