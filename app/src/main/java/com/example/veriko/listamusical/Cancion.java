package com.example.veriko.listamusical;

/**
 * Created by Veriko on 16/05/2017.
 */

public class Cancion implements java.io.Serializable {

    int portada, cancion;
    String titulo, artista, duracion;

    public Cancion(int portada, String titulo, String artista, String duracion, int cancion) {
        this.portada = portada;
        this.cancion = cancion;
        this.titulo = titulo;
        this.artista = artista;
        this.duracion = duracion;
    }

    public int getPortada() {
        return portada;
    }

    public void setPortada(int portada) {
        this.portada = portada;
    }

    public int getCancion() {
        return cancion;
    }

    public void setCancion(int cancion) {
        this.cancion = cancion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }
}
